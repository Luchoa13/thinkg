<?php
/* Smarty version 3.1.34-dev-7, created on 2020-07-20 19:24:22
  from '/home/thinkgreatnow/public_html/content/themes/thinkgreat/templates/ajax.lightbox.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5f15ef66e1bbc0_46275829',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bea988a9715c3e69994eb9175949f5d81e9a385f' => 
    array (
      0 => '/home/thinkgreatnow/public_html/content/themes/thinkgreat/templates/ajax.lightbox.tpl',
      1 => 1595272975,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:__feeds_post_photo.tpl' => 1,
  ),
),false)) {
function content_5f15ef66e1bbc0_46275829 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('post', $_smarty_tpl->tpl_vars['photo']->value['post']);?>

<div class="lightbox-post" data-id="<?php if ($_smarty_tpl->tpl_vars['photo']->value['is_single']) {
echo $_smarty_tpl->tpl_vars['post']->value['post_id'];
} else {
echo $_smarty_tpl->tpl_vars['photo']->value['photo_id'];
}?>">
	<div class="js_scroller" data-slimScroll-height="100%">
        <?php $_smarty_tpl->_subTemplateRender('file:__feeds_post_photo.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_lightbox'=>true), 0, false);
?>
	</div>
</div><?php }
}

<?php
/* Smarty version 3.1.34-dev-7, created on 2020-07-20 21:21:03
  from '/home/thinkgreatnow/public_html/content/themes/thinkgreat/templates/ajax.mutual_friends.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5f160abfdc7db5_32188258',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7dec8b2f93c93b6850896fc5d23d4e0672c9c34b' => 
    array (
      0 => '/home/thinkgreatnow/public_html/content/themes/thinkgreat/templates/ajax.mutual_friends.tpl',
      1 => 1595272980,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:__feeds_user.tpl' => 1,
  ),
),false)) {
function content_5f160abfdc7db5_32188258 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="modal-header">
    <h6 class="modal-title"><?php echo __("Mutual Friends");?>
</h6>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <ul>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['mutual_friends']->value, '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
        <?php $_smarty_tpl->_subTemplateRender('file:__feeds_user.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_tpl'=>"list",'_connection'=>"remove"), 0, true);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </ul>

    <?php if (count($_smarty_tpl->tpl_vars['mutual_friends']->value) >= $_smarty_tpl->tpl_vars['system']->value['max_results']) {?>
        <!-- see-more -->
        <div class="alert alert-info see-more js_see-more" data-get="mutual_friends" data-uid="<?php echo $_smarty_tpl->tpl_vars['uid']->value;?>
">
            <span><?php echo __("See More");?>
</span>
            <div class="loader loader_small x-hidden"></div>
        </div>
        <!-- see-more -->
    <?php }?>
</div>
<?php }
}

<?php
/* Smarty version 3.1.34-dev-7, created on 2020-07-20 17:37:28
  from '37e6e42ae2af91a9829c32cc1d9ac8ca2dad0b02' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5f15d658e01538_64563640',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f15d658e01538_64563640 (Smarty_Internal_Template $_smarty_tpl) {
?><p>Think GREAT and its team has met so many amazing individuals and organizations along our journey. To those of you that have been there along the journey, thank you for all of your time, commitment, and belief in us. To those of you just now joining our journey&hellip;.Welcome!</p>
<p>&nbsp;</p>
<p>We developed this amazing resource for so many reasons. Mostly, it is just time for all the GREAT Thinkers to be in one place, meet each other and communicate. Regardless of industry and title, it is time we all hold each other accountable to all of our goals and growth.</p>
<p>&nbsp;</p>
<p>Think Great NOW also aligns with Think GREAT&rsquo;s Mission statement by giving us the ability to "create a wave of dynamic thinking that empowers individuals and business leaders to set and accomplish their goals, no matter what circumstances they face."</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Think GREAT NOW also directly correlates with Think GREAT&rsquo;s Vision Statement by giving us the ability to stay "focused on creating a worldwide people movement. We will be the international leader in personal and professional growth by increasing business elevation, developing high levels of team engagement, and forging dynamic leaders.&rdquo;</p>
<p>&nbsp;</p>
<p>Please join us inside&hellip; There is no commitment, only a warm welcome, and a grateful thank you from Think GREAT.</p>
<p>&nbsp;</p>
<h2>Think GREAT NOW Features:</h2>
<p>&nbsp;</p>
<ul>
<li>Your GREAT Thinker profile</li>
<li>Daily new content from Think GREATs most valued content, typically reserved for in house sessions.</li>
<li>The unique opportunity to globally connect with other GREAT Thinkers within the Think GREAT Network.</li>
<li>Think GREAT NOW Groups - Create and Invite your entire team to share our private content directly into your organization's group. Empower your People with open Communication, Cultivation, and Collaboration on our favorite `content.&nbsp;</li>
<li>Book Excerpts ripped from the Think GREAT Book Collection.</li>
<li>Erik's Elite Articles - Articles Written by Think GREAT's Founder</li>
<li>Agreed Reads from Industry Leaders</li>
<li>New GREAT Thoughts</li>
<li>Growth Games</li>
<li>Mindset Motivators</li>
<li>Webinar Clips</li>
</ul><?php }
}

<?php
/* Smarty version 3.1.34-dev-7, created on 2020-07-09 00:41:05
  from '54404abfdfd2b5382dcf4e1e26131d3e9341514a' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5f0667a1114192_77672514',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f0667a1114192_77672514 (Smarty_Internal_Template $_smarty_tpl) {
?><p><strong> We run this website and permits its use according to the following terms and conditions: </strong></p>
<h3 class="text-info">Basic Terms:</h3>
<ol>
<li>Using this website implies your acceptance of these conditions. If you do not fully accept them, your entry to this site will be considered unauthorized and you will have to stop using it immediately</li>
<li>You must be 13 years or older to use this site.</li>
<li>You are responsible for any activity that occurs under your screen name.</li>
<li>You are responsible for keeping your account secure.</li>
<li>You must not abuse, harass, threaten or intimidate other ThinkGreat users.</li>
<li>You are solely responsible for your conduct and any data, text, information, screen names, graphics, photos, profiles, audio and video clips, links ("Content") that you submit, post, and display on the ThinkGreat service.</li>
<li>You must not modify, adapt or hack ThinkGreat or modify another website so as to falsely imply that it is associated with ThinkGreat</li>
<li>You must not create or submit unwanted email to any ThinkGreat members ("Spam").</li>
<li>You must not transmit any worms or viruses or any code of a destructive nature.</li>
<li>You must not, in the use of ThinkGreat, violate any laws in your jurisdiction (including but not limited to copyright laws).</li>
</ol>
<p>Violation of any of these agreements will result in the termination of your ThinkGreat account. While ThinkGreat prohibits such conduct and content on its site, you understand and agree that ThinkGreat cannot be responsible for the Content posted on its web site and you nonetheless may be exposed to such materials and that you use the ThinkGreat service at your own risk.</p>
<h3 class="text-info">General Conditions:</h3>
<ol>
<li>We reserve the right to modify or terminate the ThinkGreat service for any reason, without notice at any time.</li>
<li>We reserve the right to alter these Terms of Use at any time. If the alterations constitute a material change to the Terms of Use, we will notify you via internet mail according to the preference expressed on your account. What constitutes a "material change" will be determined at our sole discretion, in good faith and using common sense and reasonable judgement.</li>
<li>We reserve the right to refuse service to anyone for any reason at any time.</li>
<li>We may, but have no obligation to, remove Content and accounts containing Content that we determine in our sole discretion are unlawful, offensive, threatening, libelous, defamatory, obscene or otherwise objectionable or violates any party's intellectual property or these Terms of Use.</li>
<li>ThinkGreat service makes it possible to post images and text hosted on ThinkGreat to outside websites. This use is accepted (and even encouraged!). However, pages on other websites which display data hosted on ThinkGreat must provide a link back to ThinkGreat.</li>
</ol>
<h3 class="text-info">Copyright (What's Yours is Yours):</h3>
<ol>
<li>We claim no intellectual property rights over the material you provide to the ThinkGreat service. Your profile and materials uploaded remain yours. You can remove your profile at any time by deleting your account. This will also remove any text and images you have stored in the system.</li>
<li>We encourage users to contribute their creations to the public domain or consider progressive licensing terms.</li>
</ol>
<p><small> <em>Last updated on: JULY 8, 2020</em></small></p><?php }
}

<?php
/* Smarty version 3.1.34-dev-7, created on 2020-07-20 19:51:53
  from '/home/thinkgreatnow/public_html/content/themes/thinkgreat/templates/admin.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5f15f5d9273509_76379345',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a55d40471456b7901e1e673d96d2c6ccd5db6836' => 
    array (
      0 => '/home/thinkgreatnow/public_html/content/themes/thinkgreat/templates/admin.tpl',
      1 => 1595272968,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:admin.".((string)$_smarty_tpl->tpl_vars[\'view\']->value).".tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_5f15f5d9273509_76379345 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- page content -->
<div class="container mt20 offcanvas">
    <div class="row">

        <!-- left panel -->
        <div class="col-md-4 col-lg-3 offcanvas-sidebar">
            
            <!-- System -->
            <div class="card mb5">
                <div class="card-header block-title">
                    <?php echo __("System");?>

                </div>
                <div class="card-body with-nav">
                    <ul class="side-nav">
                        <!-- Dashboard -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "dashboard") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
">
                                <i class="fa fa-tachometer-alt fa-fw mr10" style="color: #3F51B5"></i><?php echo __("Dashboard");?>

                            </a>
                        </li>
                        <!-- Dashboard -->

                        <?php if ($_smarty_tpl->tpl_vars['user']->value->_is_admin) {?>
                            <!-- Settings -->
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "settings") {?>class="active"<?php }?>>
                                <a href="#settings" data-toggle="collapse" <?php if ($_smarty_tpl->tpl_vars['view']->value == "settings") {?>aria-expanded="true"<?php }?>>
                                    <i class="fa fa-cog fa-fw mr10" style="color: #3F51B5"></i><?php echo __("Settings");?>

                                </a>
                                <div class='collapse <?php if ($_smarty_tpl->tpl_vars['view']->value == "settings") {?>show<?php }?>' id="settings">
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "settings" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/settings">
                                                <i class="fa fa-cogs fa-fw mr10"></i><?php echo __("System Settings");?>

                                            </a>
                                        </li>
                                        
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "settings" && $_smarty_tpl->tpl_vars['sub_view']->value == "security") {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/settings/security">
                                                <i class="fa fa-shield-alt fa-fw mr10"></i><?php echo __("Security Settings");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "settings" && $_smarty_tpl->tpl_vars['sub_view']->value == "payments") {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/settings/payments">
                                                <i class="fa fa-credit-card fa-fw mr10"></i><?php echo __("Payments Settings");?>

                                            </a>
                                        </li>
                                        
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "settings" && $_smarty_tpl->tpl_vars['sub_view']->value == "analytics") {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/settings/analytics">
                                                <i class="fa fa-chart-pie fa-fw mr10"></i><?php echo __("Analytics Settings");?>

                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!-- Settings -->

                            <!-- Themes -->

                            <!-- Themes -->

                            <!-- Design -->
                           
                            <!-- Design -->

                            <!-- Languages -->
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "languages") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/languages">
                                    <i class="fa fa-language fa-fw mr10" style="color: #3F51B5"></i><?php echo __("Languages");?>

                                </a>
                            </li>
                            <!-- Languages -->

                            <!-- Currencies -->
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "currencies") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/currencies">
                                    <i class="fa fa-money-bill-alt fa-fw mr10" style="color: #3F51B5"></i><?php echo __("Currencies");?>

                                </a>
                            </li>
                            <!-- Currencies -->
                        <?php }?>
                    </ul>
                </div>
            </div>
            <!-- System -->

            <!-- Modules -->
            <div class="card mb5">
                <div class="card-header block-title">
                    <?php echo __("Modules");?>

                </div>
                <div class="card-body with-nav">
                    <ul class="side-nav">

                        <?php if ($_smarty_tpl->tpl_vars['user']->value->_is_admin) {?>
                            <!-- Users -->
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "users") {?>class="active"<?php }?>>
                                <a href="#users" data-toggle="collapse" <?php if ($_smarty_tpl->tpl_vars['view']->value == "users") {?>aria-expanded="true"<?php }?>>
                                    <i class="fa fa-user fa-fw mr10" style="color: #F44336"></i><?php echo __("Users");?>

                                </a>
                                <div class='collapse <?php if ($_smarty_tpl->tpl_vars['view']->value == "users") {?>show<?php }?>' id="users">
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "users" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/users">
                                                <?php echo __("List Users");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "users" && $_smarty_tpl->tpl_vars['sub_view']->value == "admins") {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/users/admins">
                                                <?php echo __("List Admins");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "users" && $_smarty_tpl->tpl_vars['sub_view']->value == "moderators") {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/users/moderators">
                                                <?php echo __("List Moderators");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "users" && $_smarty_tpl->tpl_vars['sub_view']->value == "online") {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/users/online">
                                                <?php echo __("List Online");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "users" && $_smarty_tpl->tpl_vars['sub_view']->value == "banned") {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/users/banned">
                                                <?php echo __("List Banned");?>

                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!-- Users -->
                        <?php }?>

                        <!-- Posts -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "posts") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/posts">
                                <i class="fa fa-newspaper fa-fw mr10" style="color: #F44336"></i><?php echo __("Posts");?>

                            </a>
                        </li>
                        <!-- Posts -->

                        <!-- Pages -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pages") {?>class="active"<?php }?>>
                            <a href="#pages" data-toggle="collapse" <?php if ($_smarty_tpl->tpl_vars['view']->value == "pages") {?>aria-expanded="true"<?php }?>>
                                <i class="fa fa-flag fa-fw mr10" style="color: #F44336"></i><?php echo __("Pages");?>

                            </a>
                            <div class='collapse <?php if ($_smarty_tpl->tpl_vars['view']->value == "pages") {?>show<?php }?>' id="pages">
                                <ul>
                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pages" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/pages">
                                            <?php echo __("List Pages");?>

                                        </a>
                                    </li>
                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pages" && $_smarty_tpl->tpl_vars['sub_view']->value == "categories") {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/pages/categories">
                                            <?php echo __("List Categories");?>

                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- Pages -->

                        <!-- Groups -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "groups") {?>class="active"<?php }?>>
                            <a href="#groups" data-toggle="collapse" <?php if ($_smarty_tpl->tpl_vars['view']->value == "groups") {?>aria-expanded="true"<?php }?>>
                                <i class="fa fa-users fa-fw mr10" style="color: #F44336"></i><?php echo __("Teams");?>

                            </a>
                            <div class='collapse <?php if ($_smarty_tpl->tpl_vars['view']->value == "groups") {?>show<?php }?>' id="groups">
                                <ul>
                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "groups" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/groups">
                                            <?php echo __("List Teams");?>

                                        </a>
                                    </li>
                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "groups" && $_smarty_tpl->tpl_vars['sub_view']->value == "categories") {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/groups/categories">
                                            <?php echo __("List Categories");?>

                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- Groups -->

                        <!-- Events -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "events") {?>class="active"<?php }?>>
                            <a href="#events" data-toggle="collapse" <?php if ($_smarty_tpl->tpl_vars['view']->value == "events") {?>aria-expanded="true"<?php }?>>
                                <i class="fa fa-calendar fa-fw mr10" style="color: #F44336"></i><?php echo __("Events");?>

                            </a>
                            <div class='collapse <?php if ($_smarty_tpl->tpl_vars['view']->value == "events") {?>show<?php }?>' id="events">
                                <ul>
                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "events" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/events">
                                            <?php echo __("List Events");?>

                                        </a>
                                    </li>
                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "events" && $_smarty_tpl->tpl_vars['sub_view']->value == "categories") {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/events/categories">
                                            <?php echo __("List Categories");?>

                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- Events -->

                        <!-- Blogs -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "blogs") {?>class="active"<?php }?>>
                            <a href="#blogs" data-toggle="collapse" <?php if ($_smarty_tpl->tpl_vars['view']->value == "blogs") {?>aria-expanded="true"<?php }?>>
                                <i class="fab fa-blogger-b fa-fw mr10" style="color: #F44336"></i><?php echo __("Blogs");?>

                            </a>
                            <div class='collapse <?php if ($_smarty_tpl->tpl_vars['view']->value == "blogs") {?>show<?php }?>' id="blogs">
                                <ul>
                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "blogs" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/blogs">
                                            <?php echo __("List Articles");?>

                                        </a>
                                    </li>
                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "blogs" && $_smarty_tpl->tpl_vars['sub_view']->value == "categories") {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/blogs/categories">
                                            <?php echo __("List Categories");?>

                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- Blogs -->

                        <!-- Market -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "market") {?>class="active"<?php }?>>
                            <a href="#market" data-toggle="collapse" <?php if ($_smarty_tpl->tpl_vars['view']->value == "market") {?>aria-expanded="true"<?php }?>>
                                <i class="fa fa-shopping-bag fa-fw mr10" style="color: #F44336"></i><?php echo __("Market");?>

                            </a>
                            <div class='collapse <?php if ($_smarty_tpl->tpl_vars['view']->value == "market") {?>show<?php }?>' id="market">
                                <ul>
                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "market" && $_smarty_tpl->tpl_vars['sub_view']->value == "categories") {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/market/categories">
                                            <?php echo __("List Categories");?>

                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- Market -->

                        <!-- Forums -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "forums") {?>class="active"<?php }?>>
                            <a href="#forums" data-toggle="collapse" <?php if ($_smarty_tpl->tpl_vars['view']->value == "forums") {?>aria-expanded="true"<?php }?>>
                                <i class="fa fa-comments fa-fw mr10" style="color: #F44336"></i><?php echo __("Forums");?>

                            </a>
                            <div class='collapse <?php if ($_smarty_tpl->tpl_vars['view']->value == "forums") {?>show<?php }?>' id="forums">
                                <ul>
                                    <?php if ($_smarty_tpl->tpl_vars['user']->value->_is_admin) {?>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "forums" && $_smarty_tpl->tpl_vars['sub_view']->value == "settings") {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/forums/settings">
                                                <?php echo __("Forums Settings");?>

                                            </a>
                                        </li>
                                    <?php }?>
                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "forums" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/forums">
                                            <?php echo __("List Forums");?>

                                        </a>
                                    </li>
                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "forums" && $_smarty_tpl->tpl_vars['sub_view']->value == "threads") {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/forums/threads">
                                            <?php echo __("List Threads");?>

                                        </a>
                                    </li>
                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "forums" && $_smarty_tpl->tpl_vars['sub_view']->value == "replies") {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/forums/replies">
                                            <?php echo __("List Replies");?>

                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- Forums -->

                        <!-- Movies -->
                        
                        <!-- Movies -->

                        <!-- Games -->
                        
                        <!-- Games -->

                    </ul>
                </div>
            </div>
            <!-- Modules -->

             <!-- Money -->
            <?php if ($_smarty_tpl->tpl_vars['user']->value->_is_admin) {?>
                <div class="card mb5">
                    <div class="card-header block-title">
                        <?php echo __("Money");?>

                    </div>
                    <div class="card-body with-nav">
                        <ul class="side-nav">
                            
                            <!-- Ads -->
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "ads") {?>class="active"<?php }?>>
                                <a href="#ads" data-toggle="collapse" <?php if ($_smarty_tpl->tpl_vars['view']->value == "ads") {?>aria-expanded="true"<?php }?>>
                                    <i class="fa fa-dollar-sign fa-fw mr10" style="color: #4CAF50"></i><?php echo __("Ads");?>

                                </a>
                                <div class='collapse <?php if ($_smarty_tpl->tpl_vars['view']->value == "ads") {?>show<?php }?>' id="ads">
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "ads" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/ads">
                                                <?php echo __("Ads Settings");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "ads" && $_smarty_tpl->tpl_vars['sub_view']->value == "system_ads") {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/ads/system_ads">
                                                <?php echo __("List System Ads");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "ads" && $_smarty_tpl->tpl_vars['sub_view']->value == "users_ads") {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/ads/users_ads">
                                                <?php echo __("List Users Ads");?>

                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!-- Ads -->

                            <!-- Packages -->
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "packages") {?>class="active"<?php }?>>
                                <a href="#packages" data-toggle="collapse" <?php if ($_smarty_tpl->tpl_vars['view']->value == "packages") {?>aria-expanded="true"<?php }?>>
                                    <i class="fa fa-cubes fa-fw mr10" style="color: #4CAF50"></i><?php echo __("Pro Packages");?>

                                </a>
                                <div class='collapse <?php if ($_smarty_tpl->tpl_vars['view']->value == "packages") {?>show<?php }?>' id="packages">
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "packages" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/packages">
                                                <?php echo __("List Packages");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "packages" && $_smarty_tpl->tpl_vars['sub_view']->value == "subscribers") {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/packages/subscribers">
                                                <?php echo __("List Subscribers");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "packages" && $_smarty_tpl->tpl_vars['sub_view']->value == "earnings") {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/packages/earnings">
                                                <?php echo __("Earnings");?>

                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!-- Packages -->

                            <!-- CoinPayments -->

                            <!-- CoinPayments -->

                            <!-- Bank Receipts -->
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "bank") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/bank">
                                    <?php if ($_smarty_tpl->tpl_vars['bank_transfers_insights']->value) {?><span class="float-right badge badge-pill badge-danger"><?php echo $_smarty_tpl->tpl_vars['bank_transfers_insights']->value;?>
</span><?php }?>
                                    <i class="fa fa-university fa-fw mr10" style="color: #4CAF50"></i><?php echo __("Bank Receipts");?>

                                </a>
                            </li>
                            <!-- Bank Receipts -->

                            <!-- Affiliates -->
                            
                            <!-- Affiliates -->

                            <!-- Points -->
                            
                            <!-- Points -->

                        </ul>
                    </div>
                </div>
            <?php }?>
            <!-- Money -->

            <!-- Developers -->
            
            <!-- Developers -->

            <!-- Tools -->
            <div class="card mb5">
                <div class="card-header block-title">
                    <?php echo __("Tools");?>

                </div>
                <div class="card-body with-nav">
                    <ul class="side-nav">

                        <!-- Reports -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "reports") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/reports">
                                <?php if ($_smarty_tpl->tpl_vars['reports_insights']->value) {?><span class="float-right badge badge-pill badge-danger"><?php echo $_smarty_tpl->tpl_vars['reports_insights']->value;?>
</span><?php }?>
                                <i class="fa fa-exclamation-triangle fa-fw mr10" style="color: #03A9F4"></i><?php echo __("Reports");?>

                            </a>
                        </li>
                        <!-- Reports -->

                        <?php if ($_smarty_tpl->tpl_vars['user']->value->_is_admin) {?>
                            <!-- Blacklist -->
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "blacklist") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/blacklist">
                                    <i class="fa fa-minus-circle fa-fw mr10" style="color: #03A9F4"></i><?php echo __("Blacklist");?>

                                </a>
                            </li>
                            <!-- Blacklist -->
                        <?php }?>

                        <!-- Verification -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "verification") {?>class="active"<?php }?>>
                            <a href="#verification" data-toggle="collapse" <?php if ($_smarty_tpl->tpl_vars['view']->value == "verification") {?>aria-expanded="true"<?php }?>>
                                <i class="fa fa-check-circle fa-fw mr10" style="color: #03A9F4"></i><?php echo __("Verification");?>

                            </a>
                            <div class='collapse <?php if ($_smarty_tpl->tpl_vars['view']->value == "verification") {?>show<?php }?>' id="verification">
                                <ul>
                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "verification" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/verification">
                                            <?php if ($_smarty_tpl->tpl_vars['verification_requests_insights']->value) {?><span class="float-right badge badge-pill badge-danger"><?php echo $_smarty_tpl->tpl_vars['verification_requests_insights']->value;?>
</span><?php }?>
                                            <?php echo __("List Requests");?>

                                        </a>
                                    </li>
                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "verification" && $_smarty_tpl->tpl_vars['sub_view']->value == "users") {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/verification/users">
                                            <?php echo __("List Verified Users");?>

                                        </a>
                                    </li>
                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "verification" && $_smarty_tpl->tpl_vars['sub_view']->value == "pages") {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/verification/pages">
                                            <?php echo __("List Verified Pages");?>

                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- Verification -->

                        <?php if ($_smarty_tpl->tpl_vars['user']->value->_is_admin) {?>
                            <!-- Tools -->
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "tools") {?>class="active"<?php }?>>
                                <a href="#tools" data-toggle="collapse" <?php if ($_smarty_tpl->tpl_vars['view']->value == "tools") {?>aria-expanded="true"<?php }?>>
                                    <i class="fa fa-toolbox fa-fw mr10" style="color: #03A9F4"></i><?php echo __("Tools");?>

                                </a>
                                <div class='collapse <?php if ($_smarty_tpl->tpl_vars['view']->value == "tools") {?>show<?php }?>' id="tools">
                                    <ul>
                                       
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "tools" && $_smarty_tpl->tpl_vars['sub_view']->value == "auto-connect") {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/tools/auto-connect">
                                                <?php echo __("Auto Connect");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "tools" && $_smarty_tpl->tpl_vars['sub_view']->value == "garbage-collector") {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/tools/garbage-collector">
                                                <?php echo __("Garbage Collector");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "tools" && $_smarty_tpl->tpl_vars['sub_view']->value == "backups") {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/tools/backups">
                                                <?php echo __("Backup Database & Files");?>

                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!-- Tools -->
                        <?php }?>

                    </ul>
                </div>
            </div>
            <!-- Tools -->

            <!-- Customization -->
            <?php if ($_smarty_tpl->tpl_vars['user']->value->_is_admin) {?>
                <div class="card mb5">
                    <div class="card-header block-title">
                        <?php echo __("Customization");?>

                    </div>
                    <div class="card-body with-nav">
                        <ul class="side-nav">

                            <!-- Custom Fields -->
                            
                            <!-- Custom Fields -->
                            
                            <!-- Static Pages -->
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "static") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/static">
                                    <i class="fa fa-file fa-fw mr10" style="color: #FF5722"></i><?php echo __("Static Pages");?>

                                </a>
                            </li>
                            <!-- Static Pages -->

                            <!-- Vibrant Posts -->
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "colored_posts") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/colored_posts">
                                    <i class="fa fa-palette fa-fw mr10" style="color: #FF5722"></i><?php echo __("Vibrant Posts");?>

                                </a>
                            </li>
                            <!-- Vibrant Posts -->

                            <!-- Widgets -->
                            
                            <!-- Widgets -->

                            <!-- Emojis -->
                            
                            <!-- Emojis -->

                            <!-- Stickers -->
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "stickers") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/stickers">
                                    <i class="fa fa-hand-peace fa-fw mr10" style="color: #FF5722"></i><?php echo __("Stickers");?>

                                </a>
                            </li>
                            <!-- Stickers -->

                            <!-- Gifts -->
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "gifts") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/gifts">
                                    <i class="fa fa-gift fa-fw mr10" style="color: #FF5722"></i><?php echo __("Gifts");?>

                                </a>
                            </li>
                            <!-- Gifts -->

                        </ul>
                    </div>
                </div>
            <?php }?>
            <!-- Customization -->

            <!-- Reach -->
            <?php if ($_smarty_tpl->tpl_vars['user']->value->_is_admin) {?>
                <div class="card mb5">
                    <div class="card-header block-title">
                        <?php echo __("Reach");?>

                    </div>
                    <div class="card-body with-nav">
                        <ul class="side-nav">

                            <!-- Announcements -->
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "announcements") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/announcements">
                                    <i class="fa fa-bullhorn fa-fw mr10" style="color: #009688"></i><?php echo __("Announcements");?>

                                </a>
                            </li>
                            <!-- Announcements -->

                            <!-- Notifications -->
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "notifications") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/notifications">
                                    <i class="fa fa-bell fa-fw mr10" style="color: #009688"></i><?php echo __("Mass Notifications");?>

                                </a>
                            </li>
                            <!-- Notifications -->

                            <!-- Newsletter -->
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "newsletter") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['control_panel']->value['url'];?>
/newsletter">
                                    <i class="fa fa-paper-plane fa-fw mr10" style="color: #009688"></i><?php echo __("Newsletter");?>

                                </a>
                            </li>
                            <!-- Newsletter -->

                        </ul>
                    </div>
                </div>
            <?php }?>
            <!-- Reach -->

            <!-- Sngine -->
            
            <!-- Sngine -->

        </div>
        <!-- left panel -->
        
        <!-- right panel -->
        <div class="col-md-8 col-lg-9 offcanvas-mainbar">
            <?php $_smarty_tpl->_subTemplateRender("file:admin.".((string)$_smarty_tpl->tpl_vars['view']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
        </div>
        <!-- right panel -->
        
    </div>
</div>
<!-- page content -->

<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
